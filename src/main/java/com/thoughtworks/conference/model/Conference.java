package com.thoughtworks.conference.model;

import java.io.Serializable;

/**
 * 会议时间基础
 *
 * @author Roah
 * @since 08/09/2019
 */
public class Conference implements Serializable {

    private static final long serialVersionUID = 1L;

    //会议名称
    private String name;
    //会议时间长度
    private Integer minLength;
    //用startTime来处理结束时间刚好为12点的情况，初始值为0，为1时表示该会议结束时间刚好为12点
    private Integer startTime;
    //所有会议总长度
    private Integer allConTimeLength;
    //会议所在的日期
    private Integer trackDate;
    //控制午饭输出次数，只能输出1次
    private Integer bool;
    //用于判断是否是下午的第一场会议，初始值为0
    private Integer afterMoonFirst;
    //换天提示
    private Integer overDay;

    public Integer getOverDay() {
        return overDay;
    }

    public void setOverDay(Integer overDay) {
        this.overDay = overDay;
    }

    public Integer getAfterMoonFirst() {
        return afterMoonFirst;
    }

    public void setAfterMoonFirst(Integer afterMoonFirst) {
        this.afterMoonFirst = afterMoonFirst;
    }

    //对象输出文字信息
    private String print;

    public String getPrint() {
        return print;
    }

    public void setPrint(String print) {
        this.print = print;
    }

    public Integer getBool() {
        return bool;
    }

    public void setBool(Integer bool) {
        this.bool = bool;
    }

    public Integer getTrackDate() {
        return trackDate;
    }

    public void setTrackDate(Integer trackDate) {
        this.trackDate = trackDate;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getAllConTimeLength() {
        return allConTimeLength;
    }

    public void setAllConTimeLength(Integer allConTimeLength) {
        this.allConTimeLength = allConTimeLength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public String getNameAndMin() {
        return name + " " + minLength;
    }
}
