package com.thoughtworks.conference.service;

/**
 * 会议时间表Service
 *
 * @author Roah
 * @since 08/09/2019
 */
public interface ConferenceService {

    /**
     * 执行会议计划
     */
    void plan();
}
