package com.thoughtworks.conference.service.impl;

import com.thoughtworks.conference.model.Conference;
import com.thoughtworks.conference.service.ConferenceService;
import com.thoughtworks.conference.util.FileUtils;
import com.thoughtworks.conference.util.GroupUtils;
import com.thoughtworks.conference.util.TimeConvertUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 会议时间表Service实现
 *
 * @author Roah
 * @since 08/09/2019
 */
public class ConferencePlanServiceImpl implements ConferenceService {

    @Override
    public void plan() {
        //k：会议名称，v：会议时间
        HashMap<String, Integer> totalMap = FileUtils.readConference2Map();
        //调用工具类方法返回所有会议的时间：时间去重且降序排列
        ArrayList<Integer> minList = (ArrayList<Integer>) GroupUtils.getListByMin(totalMap);
        //把totalMap中的会议按照时间长短降序排列

        ArrayList<Conference> conferenceList = GroupUtils.getConferenceList(totalMap, minList);
        //ArrayList<Conference> endList = GroupUtils.getEndList(conferenceList);
        //所有会议时间长度
        int twoDaysTime = 0;
        Conference conInfo = null;
        for (int i = 0; i < conferenceList.size(); i++) {
            //对会议时间总长度进行判断
            twoDaysTime = twoDaysTime + conferenceList.get(i).getMinLength();
        }

        if (twoDaysTime > (12 - 9 + 17 - 13) * 2 * 60) {
            System.out.println("会议总长度超出预定会议时间，请重新安排会议");
        } else {
            for (int i = 0; i < conferenceList.size(); i++) {

                conInfo = TimeConvertUtils.minConvertToHours(conferenceList.get(i));
                if (conInfo.getOverDay() == 1) {
                    System.out.println("05:00PM Networking Event");
                    conferenceList.get(i + 1).setOverDay(conInfo.getOverDay() + 1);
                }
                if (conInfo.getAllConTimeLength() == 540) {
                    System.out.println("Tranck " + conInfo.getTrackDate());
                    conferenceList.get(i + 1).setTrackDate(conInfo.getTrackDate() + 1);
                }//对最特殊的情况处理：会议刚好在12点结束的情况，先输出会议，再打印lunch
                if (conInfo.getStartTime() == 1) {
                    System.out.println(conInfo.getPrint() + conInfo.getNameAndMin());
                    System.out.println("12:00PM Lunch");
                    //并把下一个会议的AllConTimeLength属性设置为13*60
                    conferenceList.get(i + 1).setAllConTimeLength(13 * 60);
                    conferenceList.get(i + 1).setStartTime(0);
                    conferenceList.get(i + 1).setBool(1);
                    continue;
                }
                //先判断会议是不是在下午，如果是，再判断是不是第一个会议，是才输出Lunch
                //下午第一场会议永远在13点开始，此处是午餐时间判断
                if (conInfo.getAllConTimeLength() == 13 * 60) {
                    if (conInfo.getBool() == 0) {
                        System.out.println("12:00PM Lunch");
                        conferenceList.get(i + 1).setBool(conInfo.getBool() + 1);
                        conferenceList.get(i + 1).setAfterMoonFirst(conferenceList.get(i).getAfterMoonFirst() + 1);
                    }
                }
                //输出日志
                System.out.println(conInfo.getPrint() + conInfo.getNameAndMin());
                //把当前的会议的总时长，加上自身时长，赋值到下一个会议的总时长里面，避免数组越界
                if (i != conferenceList.size() - 1) {
                    conferenceList.get(i + 1).setAllConTimeLength(conferenceList.get(i).getAllConTimeLength() + conferenceList.get(i).getMinLength());
                    conferenceList.get(i + 1).setAfterMoonFirst(conferenceList.get(i).getAfterMoonFirst() + 1);
                } else {
                    //如果没有下一个对象，输出完成
                    System.out.println("05:00PM Networking Event");
                }
            }
        }
    }

}
