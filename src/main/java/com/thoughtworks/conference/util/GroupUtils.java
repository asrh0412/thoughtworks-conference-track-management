package com.thoughtworks.conference.util;


import com.thoughtworks.conference.model.Conference;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 会议时间基础
 *
 * @author Roah
 * @since 08/09/2019
 */
public class GroupUtils {
    /**
     * 返回map中value的去重数组, 并且降序排列
     * @param totalMap
     * @return
     */
    public static List<Integer> getListByMin(HashMap<String, Integer> totalMap) {

        int min = 0;
        ArrayList<Integer> minList = new ArrayList<Integer>();
        for (Map.Entry<String, Integer> entry : totalMap.entrySet()) {
            min = entry.getValue();
            minList.add(min);
        }
        //利用Java 8新特性去重
        return scort(minList.stream().distinct().collect(Collectors.toList()));
    }

    /**
     * 降序排列
     */
    public static List<Integer> scort(List<Integer> paraList) {

        Collections.sort(paraList, Collections.reverseOrder());
        return paraList;

    }

    /**
     * 返回按照会议时间长短降序排列后的Conference实体list
     */
    public static ArrayList<Conference> getConferenceList(HashMap<String, Integer> totalMap, ArrayList<Integer> minList) {

        int minLength = 0;
        ArrayList<Conference> conferenceList = new ArrayList<Conference>();
        for (int i = 0; i < minList.size(); i++) {
            //获取到最长时间、第二长时间....
            minLength = minList.get(i);

            HashMap<String, Integer> minLengthMap = new HashMap<String, Integer>();
            //minLengthMap是一个暂存map，存放所有时间长度相同的entry
            for (Map.Entry<String, Integer> entry : totalMap.entrySet()) {
                if (minLength == entry.getValue()) {
                    minLengthMap.put(entry.getKey(), entry.getValue());
                }
            }
            //此时的minLengthMap存放的是具有相同时间长度的会议
            for (Map.Entry<String, Integer> entry : minLengthMap.entrySet()) {
                Conference conferenceEntity = new Conference();
                conferenceEntity.setName(entry.getKey());
                conferenceEntity.setMinLength(entry.getValue());
                //赋值时给每个对象的startTime设置为0
                conferenceEntity.setStartTime(0);
                conferenceEntity.setTrackDate(1);
                conferenceEntity.setAllConTimeLength(540);
                conferenceEntity.setBool(0);
                conferenceEntity.setPrint(" ");
                conferenceEntity.setAfterMoonFirst(0);
                conferenceEntity.setOverDay(0);
                conferenceList.add(conferenceEntity);
            }
        }


        return conferenceList;
    }

    /**
     * 把从第二个开始的每一个对象的AllConTimeLength设置为上一个对象的总长度+上一个对象的会议时间长度
     */
    public static ArrayList<Conference> getEndList(ArrayList<Conference> conferenceList) {

        for (int i = 1; i < conferenceList.size(); i++) {
            conferenceList.get(i).setAllConTimeLength(
                    conferenceList.get(i - 1).getAllConTimeLength() + conferenceList.get(i - 1).getMinLength());
        }
        return conferenceList;
    }
}
