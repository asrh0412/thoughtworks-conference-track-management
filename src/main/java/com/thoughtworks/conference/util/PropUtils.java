package com.thoughtworks.conference.util;

import java.io.IOException;
import java.util.Properties;

/**
 * 配置工具类
 *
 * @author Roah
 * @since 08/09/2019
 */
public class PropUtils {
    private static Properties properties = new Properties();
    //加载properties中的file.properties,KV类型:conference-filepath
    static{
        try {
            properties.load(PropUtils.class.getResourceAsStream("/file.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取会议时间安排文件路径
     * @return
     */
    static String getFilePath(){
        return properties.getProperty("conference-filepath");
    }
}
