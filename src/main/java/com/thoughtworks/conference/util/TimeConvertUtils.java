package com.thoughtworks.conference.util;

import com.thoughtworks.conference.model.Conference;


/**
 * 时间转换工具类
 *
 * @author Roah
 * @since 08/09/2019
 */
public class TimeConvertUtils {

    /**
     * 根据对象信息，转换对象
     *
     * @return
     */
    public static Conference minConvertToHours(Conference entity) {

        //如果是每天第一个会议，该值为540(分钟)
        int beforeTotalTime = entity.getAllConTimeLength();
        //该场会议的时间长度
        int thisTime = entity.getMinLength();
        //两者的和，即为该场会议的结束时间
        int endTime = beforeTotalTime + thisTime;
        //会议开始的时间和分钟
        int hour = 0;
        int min = 0;

        //endTime只是用于判断该场会议的结束时间是否超出12点或者17点，min和hour用于表示该会议的开始时间，用beforeTotalTime计算
        if (endTime < 12 * 60) {
            min = beforeTotalTime % 60;
            hour = (beforeTotalTime - min) / 60;
        }//如果结束时间刚好在12点(会议在12:00结束并不影响会议开始时间)
        else if (endTime == 12 * 60) {
            min = beforeTotalTime % 60;
            hour = (beforeTotalTime - min) / 60;
            //把自身的总时长置为13*60
            entity.setStartTime(1);
        } else if (endTime > 12 * 60 && endTime <= 17 * 60) {
            if (entity.getAfterMoonFirst() == 0) {
                entity.setAllConTimeLength(13 * 60);
                //下午第一场的起始时间为13:00
                hour = 13;
                min = 0;
            } else {
                //如果不是下午第一场
                min = beforeTotalTime % 60;
                hour = (beforeTotalTime - min) / 60;
            }
        } else {
            //如果该会议的长度超过了17点，返回一个输出Networking Event的消息，并把beforeTotalTime设置540，track设置现在的track + 1
            entity.setOverDay(1);//1表示要从今天换到明天
            entity.setAllConTimeLength(540);
            entity.setTrackDate(entity.getTrackDate() + 1);
            beforeTotalTime = entity.getAllConTimeLength();
            hour = 9;
            min = 0;
        }
        //对hour,min进行处理
        String HOUR = null;
        String MIN = null;
        if (min == 0) {
            MIN = "00";
        } else if (min < 10) {
            MIN = "0" + min;
        } else {
            MIN = min + "";
        }
        switch (hour) {
            case 9:
                HOUR = "09:";
                entity.setPrint(HOUR + MIN + "AM ");
                break;
            case 10:
                HOUR = "10:";
                entity.setPrint(HOUR + MIN + "AM ");
                break;
            case 11:
                HOUR = "11:";
                entity.setPrint(HOUR + MIN + "AM ");
                break;
            case 13:
                HOUR = "01:";
                entity.setPrint(HOUR + MIN + "PM ");
                break;
            case 14:
                HOUR = "02:";
                entity.setPrint(HOUR + MIN + "PM ");
                break;
            case 15:
                HOUR = "03:";
                entity.setPrint(HOUR + MIN + "PM ");
                break;
            case 16:
                HOUR = "04:";
                entity.setPrint(HOUR + MIN + "PM ");
                break;
            default:
                return null;
        }

        return entity;
    }
}
