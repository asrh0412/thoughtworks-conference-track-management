package com.thoughtworks.conference.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * 文件工具类
 *
 * @author Roah
 * @since 08/09/2019
 */
public class FileUtils {

	 /**
	 * 用map存储会议数据，k：会议名称，v：会议时间
	 * 定义为为static方法，无需生成对象即可调用
	 */
	public static HashMap<String, Integer> readConference2Map() {
		
			HashMap<String, Integer> titleAndTime = new HashMap<String, Integer>();
	        File file = new File(PropUtils.getFilePath());
	        BufferedReader reader = null;
	        String line = null;
	        //会议时间
	        Integer numTime = 0;
	        try {
	            reader = new BufferedReader(new FileReader(file));
	            while (null !=(line = reader.readLine())) {
	            	//返回最后一个空格所在的位置，空格后面即为会议时间
	                int lastBlank = line.lastIndexOf(" ");
	                //会议名称
	                String title = line.substring(0, lastBlank);
	                //会议时间
	                String time = line.substring(lastBlank + 1);
	                //对lightning进行处理，5min
	                if (time.equals("lightning")) {
	                    numTime = 5;
	                } else {
	                    // 去掉最后的"min"
	                    numTime = Integer.parseInt(time.substring(0, time.length() - 3));
	                }
	                titleAndTime.put(title, numTime);
	            }
	            reader.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (reader != null) {
	                try {
	                    reader.close();
	                } catch (IOException e1) {
	                }
	            }
	        }
	        return titleAndTime;
	    }
}
