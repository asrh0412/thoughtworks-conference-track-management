package com.thoughtworks.conference.test;

import com.thoughtworks.conference.service.impl.ConferencePlanServiceImpl;

/**
 * 单元测试
 *
 * @author Roah
 * @since 08/08/2019
 */
public class Test {

    @org.junit.Test
    public void test() {
        ConferencePlanServiceImpl plan = new ConferencePlanServiceImpl();
        plan.plan();
    }
}
